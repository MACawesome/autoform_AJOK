#!Python3 program for automatic filling of the forms

import pandas as pd, docx, re, sys, os

def search_cat(str_object, search_list):
    # Searches through the column names
    # and return the matching name if one
    # is found. 
    temp_list = []
    for i in range(len(search_list)):
        if str_object in search_list[i]:
            temp_list.append(search_list[i])
    try:
        assert len(temp_list) == 1
    except AssertionError as e:
        print(str_object, 'found multiple or no times')
        raise e
    return (temp_list[0])

class camper:
    def __init__(self, index, dataset):
        self.camper_dat = dataset[index:index + 1]
        self.camper_dat = self.camper_dat.fillna('')
        self.column_names = list(self.camper_dat.columns.values)
        # Delineated info
        # Order of original form (2018) is followed
        self.general_info = {'firstname': self.out_in('Voornaam'),
                             'lastname': self.out_in('Familienaam'),
                             'birthdate': self.out_in('Geboorte'),
                             'gender': self.out_in('Geslacht'),
                             'campname': self.out_in('Kamp'),
                             'alreadycame': self.out_in('Reed'),
                             'homeresidency': self.out_in('Thuiswonend'),
                             'transportation': self.out_in('Vervoer')}
        self.guardian = {'facility': self.out_in('Naam voorziening'),
                         'adress': self.out_in('Adres voorziening'),
                         'name': self.out_in('Naam verantwoord'),
                         'function': self.out_in('Functie verant'),
                         'telephon': self.camper_dat['Telefoonnummer'].values[0],
                         'email': self.out_in('E-mail'),
                         'emergency_number': self.out_in('Noodnummer'),
                         'fact_adress': self.out_in('Facturatiegegevens'),
                         # 'remark': self.out_in('Algemene opmerk')
        }
        self.payment = {'mutuality': self.out_in('ziekenfondsen is het'),
                        'other_questions': self.out_in('over kortingen')}
        self.practical = {'speak_dutch': self.out_in('jongere Nederlands'),
                          'other_langs': self.out_in('welke talen spreekt'),
                          'marked_clothes': self.out_in('Merkteken kledij'),
                          'do_sport': self.out_in('[Sport]'),
                          'do_play': self.out_in('[Spel]'),
                          'do_swim': self.out_in('[Zwemmen]'),
                          'do_hike': self.out_in('[Tochten]'),
                          'do_reasons': self.out_in('Indien nee, waarom'),
                          'cannot_eat': self.out_in('niet eten'),
                          'eat_reasons': self.camper_dat['Reden'].values[0],
                          'smokes': self.out_in('jongere roken?')}
        self.medical = {'doctor': self.out_in('Naam huisarts'),
                        'doc_phone': self.out_in('nummer huisarts'),
                        'sight_ill': self.out_in('mindering gezicht'),
                        'earing_left': self.out_in('heid: links'),
                        'earing_right': self.out_in('heid: rechts'),
                        'sleep_walking': self.out_in('Slaapwandelen'),
                        'skin_cond': self.out_in('Huidaandoeningen'),
                        'cold': self.out_in('[Reuma]'),
                        'asthma': self.out_in('[Astma]'),
                        'epilepsy': self.out_in('[Epilepsie]'),
                        'heart_cond': self.out_in('[Hartkwaal]'),
                        'diabetes': self.out_in('[Suikerziekte]'),
                        'handicap': self.out_in('ledematen'),
                        'cold_sensi': self.out_in('verkoudheden]'),
                        'tired_sensi': self.out_in('vermoeidheid]'),
                        'bed_watering': self.out_in('[Bedwateren]'),
                        'freq_watering': self.out_in('graad bedwateren'),
                        'allergy_med': self.out_in('allergie voor genees'),
                        'which_med': self.get_exact('Indien ja, welke?'), 
                        'other_allergy': self.out_in('andere allergie'),
                        'which_allergy': self.out_in('Allergie genees'),
                        'bad_hygiene': self.out_in('[Gebrekkige persoon'),
                        'tetanus_shot': self.out_in('tetanus (klem)'),
                        'date_shot': self.out_in('datum inenting'),
                        'time_bfr_sleep': self.out_in('voor het slapen moet'),
                        'other_med': self.out_in('medicatie nodig op')}

        # Preparing the medicine table with a dictionary
        # of the following elements
        medicines = []
        dosages = []
        applications = []
        remarks = []
        for i in range(5):
            if i == 0:
                medicines.append(self.get_exact('Naam medicijn'))
                dosages.append(self.get_exact('Dosis'))
                applications.append(self.get_exact('Wijze toedienen'))
                remarks.append(self.get_exact('Opmerkingen'))
            else :
                medicines.append(self.get_exact('Naam medicijn.' + str(i)))
                dosages.append(self.get_exact('Dosis.' + str(i)))
                applications.append(self.get_exact('Wijze toedienen.' + str(i)))
                remarks.append(self.get_exact('Opmerkingen.' + str(i))) 

        med_dict = {'med_name': medicines,
                    'dos_quant': dosages,
                    'appli_mode': applications,
                    'remarks': remarks}
        col_order = ['med_name', 'dos_quant', 'appli_mode', 'remarks']
        self.medtable = pd.DataFrame(med_dict, columns=col_order)

        # Behavior of camper
        self.behavior = {# 'more_info': self.out_in('extra telefonische'),
                         'mental_level': self.out_in('Mentaal niveau jon'),
                         'group_behave': self.out_in('zich in groep'),
                         'authority_behave': self.out_in('gezagspersonen'),
                         'tips': self.out_in('aanpak en gedrag'),
                         'home_situation': self.out_in('thuissituatie'),
                         'phone_meet': self.out_in('teren we u graag om samen'),
                         'phonenumber': self.out_in('op dit nummer:'),
                         'availability1': self.get_exact('en liefst tussen'),
                         'availability2': self.get_exact('en'),
                         'leader_meeting': self.out_in('kampleiding al eens'),
                         'phonenumber2': self.out_in('op dit nummer:')}

        # Photography permissions, evaluation
        self.photos = {'permi_photo': self.out_in('kampfoto\'s van de'),
                       'permi_video': self.out_in('videobeelden van de')}
        self.evaluation = {'want_eval': self.out_in('korte evaluatie'),
                           'send_email': self.out_in('mag worden verstuurd')}
        
        # Questions to the camper
        self.questions = {'general_remarks': self.get_exact('Algemene opmerkingen of vragen'),
                          'happy_with': self.out_in('blij met...'),
                          'good_day': self.out_in('mij geslaagd als'),
                          'diff_with': self.out_in('het moeilijk met...'),
                          'helped_by': self.out_in('me zo helpen..'),
                          'most_fun': self.out_in('leukste op kamp'),
                          'to_monitor': self.out_in('aan de monitoren'),
                          'know_more': self.out_in('Wat zou je graag nog weten voor je op kamp vertrekt?')}

        # Dictionary with all available info except the medtable
        info_list = [self.general_info, self.guardian, self.payment,
                     self.practical, self.medical, self.behavior,
                     self.photos, self.evaluation, self.questions]
        self.alldict = {}
        for info in info_list:
            for key in info.keys():
                self.alldict[key] = info[key]

        # Last and first names as identifiants
        temp_last = re.sub('\s', '', self.general_info['lastname'])
        temp_first = re.sub('\s', '', self.general_info['firstname'])
        self.idname = temp_last + '_' + temp_first
        
    def out_in(self, str_object):
        # quick call to search_cat
        # using present camper_dat
        current_dat = self.camper_dat
        current_col = self.column_names
        current_output = current_dat[search_cat(str_object, current_col)]
        return (current_output.values[0])

    def get_exact(self, exact_str):
        # Obtain self data using exact
        # column name. Return seeked data or a key error
        try:
            val_output = self.camper_dat[exact_str].values[0]
        except KeyError as e:
            print(exact_str, 'not found')
            raise e
        return (val_output)

    
class form():
    def __init__(self, camper, form_name):
        self.form = docx.Document(form_name)
        self.camper = camper
        self.fill_in()
        self.fill_medtable()

    def fill_in(self):
        # Fill in the entries for the camper
        # in bold. 
        count = 0
        list_camper = list(self.camper.alldict.values())
        for i in range(len(self.form.paragraphs)):
            if (':' in self.form.paragraphs[i].text and
                ': stuur' not in self.form.paragraphs[i].text):
                self.form.paragraphs[i].text = \
                    ''.join(c for c in self.form.paragraphs[i].text
                            if not c in '\t')
                self.form.paragraphs[i].text = \
                    re.sub(r":[ ]*", ":   ", self.form.paragraphs[i].text)
                self.form.paragraphs[i].add_run(str(list_camper[count]))
                last_run = len(self.form.paragraphs[i].runs) - 1
                self.form.paragraphs[i].runs[last_run].bold = True
                count += 1

    def fill_medtable(self):
        table = self.form.tables[0]
        colnames = self.camper.medtable.columns
        panda_tab = self.camper.medtable
        for row in range(1, len(panda_tab) + 1):
            for col in range(1, len(colnames) + 1):
                table.cell(row, col).text = str(panda_tab.iloc[row-1, col-1])

        
    def output_docx(self, Verbose = True):
        # Output a docx file in working directory.
        # the file name corresponds to the camper id
        # plus the form indicator. Verbose adds a confirmation
        output_name = self.camper.idname + '_form.docx'
        self.form.save(output_name)
        if Verbose:
            print('File for', self.camper.idname, 'added to wd')

def main(argv):
    # First argument is camper csv file
    # Second argument is form emplacement
    # Output the docx files for each camper
    clean_folder = 'cleanup'
    regexp_clean = re.compile('[a-zA-Z]+_[a-zA-Z]+_form.docx')
    if sys.argv[1] == clean_folder:
        for file in os.listdir():
            check = regexp_clean.match(file)
            if not check is None:
                os.remove(file)
    else: 
        camper_dat = pd.read_csv(sys.argv[1])
        form_empl = sys.argv[2]
        try:
            start_empl = int(sys.argv[3])
        except IndexError:
            start_empl = 1
        assert start_empl >= 1
        for i in range(start_empl - 1, len(camper_dat)):
            tcamper = camper(i, camper_dat)
            tform = form(tcamper, form_empl)
            tform.output_docx()
    
if __name__ == "__main__":
    main(sys.argv)
    


# Debugging garbage    
# camper_dat = pd.read_csv('inschriving_2019.csv')

# thing = camper_dat[1:2]
# tryout = list(thing.columns.values)

