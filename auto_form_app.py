from auto_form import *
import tkinter as tk
import tkinter.messagebox
import pandas as pd

class formApp():
    # GUI following the rules of the commandline utility
    # in auto_form.py
    def __init__(self):
        self.root = tk.Tk()
        self.root.wm_title('Ajok vormen')
        self.root.minsize(width=300, height=400)

        # Show Ajok background
        C = tk.Canvas(self.root, bg="blue", height=40, width=30)
        filename = tk.PhotoImage(file = "ajok_symbol.png")
        background_label = tk.Label(self.root, image=filename)
        background_label.place(x=0, y=100, relwidth=1, relheight=1)
        
        # Data stuff
        self.labeldata = tk.Label(self.root,
                                  text='Voer de naam van het csv-bestand in: ')
        self.labeldata.pack()
        self.entrydata = tk.StringVar()
        tk.Entry(self.root, textvariable = self.entrydata).pack()

        # Form stuff
        self.labelform = tk.Label(self.root,
                                  text='Voer de naam van het lege docx document in: ')
        self.labelform.pack()
        self.entryform = tk.StringVar()
        tk.Entry(self.root, textvariable = self.entryform).pack()

        # Line stuff
        self.labelline = tk.Label(self.root,
                                  text='Voer het nummer van de eerste aanvrager in: ')

        self.labelline.pack()
        self.entryline = tk.IntVar()
        tk.Entry(self.root, textvariable = self.entryline).pack()

        self.labelconfirm = tk.Label(self.root, text='')
        self.labelconfirm.pack()

        # Extract button
        self.buttonact = tk.StringVar()
        self.buttonact.set('Extract')
        tk.Button(self.root, textvariable=self.buttonact,
                  command=self.extractclick).pack()
        
        self.root.mainloop()
        
    def extractclick(self):
        # Behavior of the extract button,
        # see command line utility
        camper_dat = pd.read_csv(self.entrydata.get())
        form_empl = self.entryform.get()
        start_empl = self.entryline.get()
        try:
            assert start_empl >= 1 and start_empl <= len(camper_dat) + 1
        except AssertionError:
            start_empl = 1
            tk.messagebox.showinfo('Fout:', 'Ongeldige startlijn')
            return None
        for i in range(start_empl - 1, len(camper_dat)):
            tcamper = camper(i, camper_dat)
            tform = form(tcamper, form_empl)
            tform.output_docx()
        success = 'Vormen geproduceerd'
        self.labelconfirm.configure(text=success)

formApp()
